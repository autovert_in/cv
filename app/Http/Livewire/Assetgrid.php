<?php

namespace App\Http\Livewire;

use App\Models\smassets;
use Livewire\Component;

class Assetgrid extends Component
{
    public $a;

    public function render()
    {
        $this->a = smassets::all();
        return view('livewire.assetgrid');
    }
}
