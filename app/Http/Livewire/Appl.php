<?php

namespace App\Http\Livewire;

use App\Models\Applications;
use Livewire\Component;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Appl extends Component
{
    use WithFileUploads;
    public $a;

    public $autovert_id="";
    public $name="";
    public $email="";
    public $phone="";
    public $nameofcompany="";
    public $companytype="";
    public $revenue="";
    public $panno="";
    public $dob="";
    public $pangender="";
    public $pincode="";
    public $bankstatementfile;
    public $pancardfile;
    public $companypancardfile;
    public $aadharfile;

    public function mount()
    {
        $id = Applications::max("id");
        if (is_null($id) or $id == 0) {
            $id = 0;
        }
        $id = $id + 1;

        $this->autovert_id = "BLRSMAL" . $id ;
    }

    public function render()
    {
        return view('livewire.appl');
    }


    public function save()
    {

        $bankstatementfilename = $this->bankstatementfile->store('/', 'docs');
        $pancardfilename = $this->pancardfile->store('/', 'docs');
        $companypancardfilename = $this->companypancardfile->store('/', 'docs');
        $aadharfilename = $this->aadharfile->store('/', 'docs');

        $mformdata = '{"vehicles" : ' . json_encode(session('vehicles')) . "," .
                '"totalassetcost" : ' . session('totalassetcost') . "," .
                '"marginmoney" : ' . session('marginmoney') . "," .
                '"minmarginmoneyline" : "' . session('minmarginmoneyline') . '",' .
                '"tenure" : ' . session('tenure') . "," .
                '"emi" : ' . session('emi') . "," .
                '"principalamount" : ' . session('principalamount') .
                "}";
        $data = [
            'autovert_id' => $this->autovert_id,
            'applicationtype' => 'loan',
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'nameofcompany' => $this->nameofcompany,
            'companytype' => $this->companytype,
            'revenue' => $this->revenue,
            'panno' => $this->panno,
            'dob' => $this->dob,
            'pangender' => $this->pangender,
            'pincode' => $this->pincode,
            'bankstatementfile' => $bankstatementfilename,
            'pancardfile' => $pancardfilename,
            'companypancardfile' => $companypancardfilename,
            'aadharfile' => $aadharfilename,
            'formdata' => $mformdata,
            'principalamount' => session('principalamount')
        ];




        $rec = Applications::updateOrCreate(["autovert_id"=>$this->autovert_id], $data);

        return redirect()->to('/formsubmitted');
    }
}
