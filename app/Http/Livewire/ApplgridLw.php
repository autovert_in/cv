<?php

namespace App\Http\Livewire;

use App\Models\Applications;
use Livewire\Component;

class ApplgridLw extends Component
{
    public $a;

    public function render()
    {
        $this->a = Applications::all();
        return view('livewire.applgrid-lw');
    }
}
