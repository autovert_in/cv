<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Offers extends Component
{
    public $offers = [
        ["status"=>"Success",
        "message"=>"You are now eligible for a loan towards building your EV Fleet!",
        "imglink"=>"img/bank-of-baroda.png",
        "Amount" => "1248505",
        "IRR" => "19%",
        "Tenure" => "36",
        "Margin Required" => "320000",
        "EMI" => "54449",
        "Charges" => "2000"
                    ],
        ["status"=>"Pending",
        "message"=>"We are still working on your application!",
        "imglink"=>"img/deutsche-bank.svg",
        "Amount" => "1248505",
        "IRR" => "19%",
        "Tenure" => "36",
        "MarginRequired" => "320000",
        "EMI" => "54449",
        "Charges" => "2000"
        ],
        ["status"=>"Rejected",
        "message"=>"Sorry! We are not in a position to move forward on your application",
        "imglink"=>"img/ujjivan-logo.jpg",
        "Amount" => "1248505",
        "IRR" => "19%",
        "Tenure" => "36",
        "MarginRequired" => "320000",
        "EMI" => "54449",
        "Charges" => "2000"
        ]
];
    public function render()
    {
        return view('livewire.offers');
    }

    public function accept()
    {
        redirect()->to('/applaccepted');
    }
}
