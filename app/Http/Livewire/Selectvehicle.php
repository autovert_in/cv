<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Selectvehicle extends Component
{
    public $vehicles = [
        ["id"=>1,
        "name"=>"E-City Fx",
        "imglink"=>"/img/Vehicle_1.png",
        "exshowroom"=>282302.00,
        "qty"=>0
        ],
        ["id"=>2,
        "name"=>"E-Xtra Fx",
        "imglink"=>"/img/Vehicle_2.png",
        "exshowroom"=>313701.00,
        "qty"=>0
        ],
        ["id"=>3,
        "name"=>"E-City",
        "imglink"=>"/img/Vehicle_3.png",
        "exshowroom"=>311711.00,
        "qty"=>0
        ]
    ];
    public $totalassetcost=0;
    protected $listeners = ['refreshtotalassetcost','refreshprincipalamount', 'refreshemi'];
    public $marginmoney=0;
    public $minmarginmoneyline="minimum 20% of asset cost";
    public $tenure=24;
    public $emi=0;
    public $principalamount=0;
    public function updated()
    {
        $this->emit('refreshtotalassetcost');
        $this->emit('refreshprincipalamount');
        $this->emit('refreshemi');
    }

    public function refreshtotalassetcost()
    {
        $this->totalassetcost = 0;
        foreach ($this->vehicles as $vehicle) {
            $q =  (int)$vehicle['qty'];
            if (!is_int($q)) {
                $q = 0;
            }
            $this->totalassetcost += ($vehicle['exshowroom'] * $q);
        }
        $this->minmarginmoneyline = "minimum 20% = ₹".round($this->totalassetcost*.20, 0);
    }

    public function refreshprincipalamount()
    {
        $mmoney = $this->marginmoney;
        if ($mmoney < $this->totalassetcost*.20) {
            $mmoney = $this->totalassetcost*.20;
        }
        $this->principalamount = round($this->totalassetcost - $mmoney, 0);
    }

    public function refreshemi()
    {
        // const emi =
        // (principalAmount + (principalAmount * (roi / 100) * tenure) / 12) /
        // tenure;


        session([
         'vehicles' => $this->vehicles,
         'totalassetcost' => $this->totalassetcost,
         'marginmoney' => $this->marginmoney,
         'minmarginmoneyline' => $this->minmarginmoneyline,
         'tenure' => $this->tenure,
         'emi' => $this->emi,
         'principalamount' => $this->principalamount
        ]);

        $this->emi = round(($this->principalamount + ($this->principalamount * (19 / 100) * $this->tenure) / 12) /$this->tenure, 0);
    }

    public function render()
    {
        return view('livewire.selectvehicle');
    }
}
