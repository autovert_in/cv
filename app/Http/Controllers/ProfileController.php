<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Http;

class ProfileController extends Controller
{
    public function panno()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://test.zoop.one/api/v1/in/identity/pan/lite',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "data": {
                "customer_pan_number": "AAIPS7371J",
                "consent": "Y",
                "consent_text" : "I hear by declare my consent agreement for fetching my information via ZOOP API."
            }
        }',
            CURLOPT_HTTPHEADER => array(
                'api-key: HV2A78B-F374K6B-GFJTFBS-W7DV3A8',
                'app-id: 61bc7660ca6adc001e2df3f3',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
        echo "<br>done<br>";
        return response()->json($response, $response['user_full_name']);
    }

    public function adhaar()
    {
        echo "adhaar";
    }

    public function bankdetails()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://test.zoop.one/api/v1/in/financial/bav/lite',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
            "data": {
                "account_number": "8045223835",
                "ifsc": "KKBK0008124",
                "consent": "Y",
                "consent_text" : "I hear by declare my consent agreement for fetching my information via ZOOP API"

                }
            }',
            CURLOPT_HTTPHEADER => array(
                'api-key: HV2A78B-F374K6B-GFJTFBS-W7DV3A8',
                'app-id: 61bc7660ca6adc001e2df3f3',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }
}
