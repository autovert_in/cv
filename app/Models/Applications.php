<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    use HasFactory;
    protected $fillable = ["autovert_id","applicationtype","name","email","phone","nameofcompany","companytype","revenue","panno","dob","pangender","pincode","bankstatementfile","pancardfile","companypancardfile","aadharfile","formdata","status","principalamount"];
}
