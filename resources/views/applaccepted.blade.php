<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Offer acceptance recorded') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-gray-50 px-14 py-14 ml-14 overflow-hidden shadow-xl sm:rounded-lg">

                <div class="grid grid-cols-3 gap-4 text-2xl font-extrabold">
                    <div class="flex justify-center ">
                        Congratulations!
                    </div>
                    <div class="text-lg"> We will get in touch with you for remaining formalities.</div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
