{{-- FilePond.registerPlugin(FilePondPluginImagePreview); --}}
{{-- labelIdle:'Veuillez sélectionner un fichier', --}}
<div
    wire:ignore
    x-data="{pond: null}"
    x-init="
        pond = FilePond.create($refs.input);
        FilePond.registerPlugin(FilePondPluginImagePreview);
pond.setOptions({
            allowMultiple: {{ isset($attributes['multiple']) ? 'true' : 'false' }},
            labelFileProcessingComplete: 'Upload complete',
            server: {
                process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                    @this.upload('{{ $attributes['wire:model'] }}', file, load, error, progress)
                },
                revert: (filename, load) => {
                    @this.removeUpload('{{ $attributes['wire:model'] }}', filename, load)
                },
            },
        });
">
    <input type="file" name="{{ $attributes['inputname'] }}" x-ref="input" credits=false>
</div>


{{-- <div wire:ignore x-data="{pond:null}" x-init="
pond = FilePond.create($refs.input);
pond.setOptions({
    allowMultiple: {{ isset($attributes['multiple']) ? 'true' : 'false' }},
    labelIdle:{{ isset($attributes['avmessage']) ? $attributes['avmessage'] : 'Drag & drop your files or click to browse'}},
    labelFileProcessingComplete: 'Successfully uploaded on Autovert server',
    server: {
        process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
            @this.upload('{{ $attributes['wire:model'] }}', file, load, error, progress)
        },
        revert: (filename, load) => {
            @this.removeUpload('{{ $attributes['wire:model'] }}', filename, load)
        },
    },
});"
>
   <input type="file" x-ref="input" credits="false">
</div>
 --}}
