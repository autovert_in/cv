<div class="flex rounded-md shadow-sm">
    <span class="inline-flex items-center px-3 rounded-1-md border border-r-0 borde-gray-300 bg-gray-50 text-gray">
        surge.com/
    </span>
    <input wire:model="username" id="username" class="flex-1 form-input block w-full rounded-none rounded-r-md" />
</div>
