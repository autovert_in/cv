<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Credit Application In Process') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-gray-50 px-14 py-14 ml-14 overflow-hidden shadow-xl sm:rounded-lg">
                <span class="mt-10 text-green-700 text-xl font-bold"> Your application is under process!</span><br>
                <span class="text-lg text-gray-600 mb-10">Next step: We will message you when offers for you are ready.</span>
            </div>
        </div>
    </div>
</x-app-layout>
