<div wire:ignore x-data="{open:false}">
    {{-- <table class="table-auto pl-10 text-xs content-center"> --}}
    <table id="assettable" class="display" style="width:100%">
        <thead class="bg-gray-200 text-sm font-bold overflow-hidden shadow-sm sm:rounded-lg">
            <tr>
            <th>Autovert id</th>
            <th>Created</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Company</th>
            <th>Credit required</th>
            <th>Status</th>
            {{-- <th>View</th> --}}
            </tr>
        </thead>
        <tbody>
            @foreach($a as $asset)
            <tr>
                <td class="text-sm">{{$asset->autovert_id}}</td>
                <td class="text-sm">{{$asset->created_at}}</td>
                <td class="text-sm">{{$asset->name}}</td>
                <td class="text-sm">{{$asset->email}}</td>
                <td class="text-sm">{{$asset->phone}}</td>
                <td class="text-sm">{{$asset->nameofcompany}}</td>
                <td class="text-sm">{{$asset->principalamount}}</td>
                <td class="text-xs">{{$asset->status}}</td>
                {{-- <td class="text-xs">
                    <div class="inline-flex">
                        <button @click="open=true" class="bg-red-700 hover:bg-gray-400 text-white px-2 font-bold rounded-l">
                          View
                        </button>
                      </div>
                </td> --}}
            </tr>
            @endforeach
        </tbody>
    </table>

    {{-- MODAL START HERE --}}
    <!-- Button trigger modal -->
    <!-- Modal -->

    <div  aria-labelledby="warningModalLabel"
        aria-hidden="true">
        <div x-show="open" @click.away = "open=false">
            <div class="bg-yellow-500">
                <div class="flex text-2xl">
                    <h5><strong>YOU ARE ABOUT TO START DEFAULT
                            MANAGEMENT</strong></h5>
                    <button type="button" @click = "open=false" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="text-lg">
                    Please note this will trigger a default management where the asset may be disabled.
                    Are you sure ?
                </div>
                <div class="bg-gray-400 px-4 py-4">
                    <x-jet-button @click="open=false" class="bg-green-700 text-white" data-dismiss="modal">Wait, not
                        now</x-jet-button>
                    <x-jet-danger-button  class="bg-red-700 text-white ">Am sure proceed with default
                        management</x-jet-danger-button>
                </div>
            </div>
        </div>
    </div>

    {{-- MODAL E N D HERE --}}

</div>
<script>
    $(document).ready(function() {
    $('#assettable').DataTable();
} );
</script>
