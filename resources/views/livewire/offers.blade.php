<div class="grid grid-cols-3 gap-4 text-2xl font-extrabold">
    @foreach ($offers as $offer )
    <div class="flex justify-center ">
        {{ $offer["status"]}}
    </div>
    @endforeach
</div>

<div class="grid grid-cols-3 gap-4 mt-8 text-sm font-normal">
    @foreach ($offers as $offer )
    <div class="flex justify-center ">
        {{ $offer["message"]}}
    </div>
    @endforeach
</div>

<div class="grid grid-cols-3 gap-4  mt-8">
    @foreach ($offers as $offer)
    <div class="flex justify-center">
        <img class="h-12" src={{ $offer["imglink"]}} />
    </div>
    @endforeach
</div>

<div class="grid grid-cols-3 gap-4  mt-8">
    @foreach ($offers as $offer)
    @if ( $offer["status"] == "Success")
    <div class="ml-8 grid grid-cols-2">
        <div>Amount:</div><div>{{ $offer["Amount"]}}</div>
        <div>IRR:</div><div> {{ $offer["IRR"]}} </div>
        <div>Tenure:</div><div> {{ $offer["Tenure"]}} </div>
        <div>Margin Required:</div><div>{{ $offer["Margin Required"]}} </div>
        <div>EMI:</div><div>{{ $offer["EMI"]}} </div>
        <div class="col-span-2 mt-6 ml-3 px-5">
        <x-jet-button onclick="location.href='{{ url('applaccepted') }}'">Accept Offer</x-jet-button></div>
    </div>
    @endif
    @endforeach
</div>
