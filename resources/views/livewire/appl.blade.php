<div class="pl-10 pr-10 content-center">
    <div class="mt-3 pl-3">
        <x-jet-form-section submit="save">
            <x-slot name="title">
                {{ __('Personal information') }}
            </x-slot>

            <x-slot name="description">
                {{ __('Basic information, which will be used for KYC and credit scoring purposes') }}
            </x-slot>

            <x-slot name="form">

                <!-- Name -->
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="name" value="{{ __('Name') }}" />
                    <x-jet-input wire:model.lazy="name" id="name" type="text" class="mt-1 block w-full" />
                    <x-jet-input-error for="name" class="mt-2" />
                </div>

                <!-- Email -->
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="email" value="{{ __('Email') }}" />
                    <x-jet-input wire:model.lazy="email" id="email" type="email" class="mt-1 block w-full" />
                    <x-jet-input-error for="email" class="mt-2" />
                </div>

                <!-- Phone -->
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="phone" value="{{ __('Phone') }}" />
                    <x-jet-input wire:model.lazy="phone" id="phone" type="text" class="mt-1 block w-full" />
                    <x-jet-input-error for="phone" class="mt-2" />
                </div>

            </x-slot>

            {{-- <x-slot name="actions">
                <x-jet-action-message class="mr-3" on="saved">
                    {{ __('Saved.') }}
                </x-jet-action-message>

                <x-jet-button>
                    {{ __('Save') }}
                </x-jet-button>
            </x-slot> --}}
        </x-jet-form-section>
    </div>

    {{-- section break --}}

    <div class="pl-3 pt-3">
        <x-jet-form-section submit="save">
            <x-slot name="title">
                {{ __('Business information') }}
            </x-slot>

            <x-slot name="description">
                {{ __('Business related information') }}
            </x-slot>

            <x-slot name="form">

                <!-- Name -->
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="name" value="{{ __('Name of company') }}" />
                    <x-jet-input wire:model.lazy="nameofcompany" id="nameofcompany" type="text"
                        class="mt-1 block w-full" autocomplete="name" />
                    <x-jet-input-error for="name" class="mt-2" />
                </div>

                <!-- type -->
                <div class=" col-span-6 sm:col-span-4">
                    <x-jet-label value="{{ __('Type of company')}}"></x-jet-label>
                </div>
                <div class="flex col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="companytype" type="radio" checked name="isCompany" id="iscompany_no"
                            class="form-check-input" value="proprietorship">
                        <x-jet-label for="iscompany_no">{{ __('Proprietorship') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="companytype" type="radio" name="isCompany" id="iscompany_no"
                            class="form-check-input" value="partnership">
                        <x-jet-label for="iscompany_no">{{ __('Partnership') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="companytype" type="radio" name="isCompany" id="iscompany_no"
                            class="form-check-input" value="pvtltd">
                        <x-jet-label for="iscompany_no">{{ __('Pvt. Ltd.') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="companytype" type="radio" name="isCompany" id="iscompany_no"
                            class="form-check-input" value="publicltd">
                        <x-jet-label for="iscompany_no">{{ __('Public Ltd.') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="companytype" type="radio" name="isCompany" id="iscompany_no"
                            class="form-check-input" value="llp">
                        <x-jet-label for="iscompany_no">{{ __('LLP') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="companytype" type="radio" name="isCompany" id="iscompany_no"
                            class="form-check-input" value="unregistered">
                        <x-jet-label for="iscompany_no">{{ __('Unregistered') }}</x-jet-label>
                    </div>
                </div>

                <!-- Revenue -->
                <div class=" col-span-6 sm:col-span-4">
                    <x-jet-label value="{{ __('Revenue in last year')}}"></x-jet-label>
                </div>
                <div class="flex col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompanyz"
                            class="form-check-input" value="0-3">
                        <x-jet-label for="iscompany">{{ __('0-3 Lacs') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompany"
                            class="form-check-input" value="3-12">
                        <x-jet-label for="iscompany">{{ __('3-12 Lacs') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompany"
                            class="form-check-input" value="12-30">
                        <x-jet-label for="iscompany">{{ __('12-30 Lacs') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompany"
                            class="form-check-input" value="30-60">
                        <x-jet-label for="iscompany">{{ __('30-60 Lacs') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompany"
                            class="form-check-input" value="60L-1Crore">
                        <x-jet-label for="iscompany">{{ __('60L-1Crore') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompany"
                            class="form-check-input" value="1-10Crores">
                        <x-jet-label for="iscompany">{{ __('1-10Crores') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input wire:model="revenue" type="radio" name="revenuegrp" id="iscompany"
                            class="form-check-input" value="above 10Crores">
                        <x-jet-label for="iscompany">{{ __('above 10Crores') }}</x-jet-label>
                    </div>
                </div>



            </x-slot>

            {{-- <x-slot name="actions">
                <x-jet-action-message class="mr-3" on="saved">
                    {{ __('Saved.') }}
                </x-jet-action-message>

                <x-jet-button>
                    {{ __('Save') }}
                </x-jet-button>
            </x-slot> --}}
        </x-jet-form-section>
    </div>

    {{-- Section break --}}

    <div class="pl-3 pt-3 pb-3">
        <x-jet-form-section submit="save">
            <x-slot name="title">
                {{ __('PAN details') }}
            </x-slot>

            <x-slot name="description">
                {{ __('requred for checking eligibility using credit score') }}
            </x-slot>

            <x-slot name="form">


                <!-- PAN -->
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="panno" value="{{ __('Personal PAN number') }}" />
                    <x-jet-input wire:model="panno" type="text" class="mt-1 block w-full" />
                    <x-jet-input-error for="panno" class="mt-2" />
                </div>

                <!-- DOB -->
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="dob" value="{{ __('Date of birth dd/mm/yyyy') }}" />
                    <x-jet-input wire:model="dob" type="text" class="mt-1 block w-full" autocomplete="dob" />
                    <x-jet-input-error for="dob" class="mt-2" />
                </div>

                <!-- Gender -->

                <div class=" col-span-6 sm:col-span-4">
                    <x-jet-label value="{{ __('Gender')}}"></x-jet-label>
                </div>
                <div class="flex col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <input type="radio" checked name="gendergrp" wire:model="pangender" class="form-check-input"
                            value="yes">
                        <x-jet-label for="iscompany_yes">{{ __('Male') }}</x-jet-label>
                    </div>
                    <div class="form-group col-md-1 pl-4">
                        <input type="radio" name="gendergrp" wire:model="pangender" class="form-check-input" value="no">
                        <x-jet-label for="iscompany_no">{{ __('Female') }}</x-jet-label>
                    </div>
                </div>


                <!-- PIN code -->
                <div class="col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <x-jet-label for="pincode" value="{{ __('Pincode') }}" />
                        <x-jet-input wire:model="pincode" type="text" class="mt-1 block w-full" />
                        <x-jet-input-error for="pincode" class="mt-2" />
                    </div>
                </div>
            </x-slot>

            {{-- <x-slot name="actions">
                <x-jet-action-message class="mr-3" on="saved">
                    {{ __('Saved.') }}
                </x-jet-action-message>

                <x-jet-button>
                    {{ __('Save') }}
                </x-jet-button>
            </x-slot> --}}
        </x-jet-form-section>
    </div>


    <div class="pl-3 pt-3 pb-3">
        <x-jet-form-section submit="save">
            <x-slot name="title">
                {{ __('Document Upload') }}
            </x-slot>
            <x-slot name="description">
                {{ __('These documents will be used for KYC and credit purposes') }}
            </x-slot>

            <x-slot name="form">
                <div style="display:none" class="col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <x-jet-label for="upload" value="{{ __('Personal PAN card image ')  }}" />
                        <x-filepond multiple inputname="a" wire:model="a" />
                    </div>
                </div>

                <div class="col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <x-jet-label for="upload" value="{{ __('Personal PAN card image ')  }}" />
                        <x-filepond multiple inputname="pancardfile" wire:model="pancardfile" />
                    </div>
                </div>
                <div class="col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <x-jet-label for="upload"
                            value="{{ __('Bank Statement from ') . \Carbon\Carbon::today()->subyear(1)->format('F Y') . ' to ' . \Carbon\Carbon::today()->format('F Y')  }}" />
                        <x-filepond multiple inputname="bankstatementfile" wire:model="bankstatementfile" />
                    </div>
                </div>
                <div class="col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <x-jet-label for="upload" value="{{ __('Company PAN card image ')  }}" />
                        <x-filepond multiple inputname="companypancardfile" wire:model="companypancardfile" />
                    </div>
                </div>
                <div class="col-span-6 sm:col-span-4">
                    <div class="form-group col-md-1 pl-4">
                        <x-jet-label for="upload" value="{{ __('Aadhar card image front and back') }}" />
                        <x-filepond multiple inputname="aadharfile" wire:model="aadharfile" />
                    </div>
                </div>
            </x-slot>
            <x-slot name="actions">
                {{-- <x-jet-action-message class="mr-3" on="saved">
                    {{ __('Saved.') }}
                </x-jet-action-message> --}}

                <x-jet-button>
                    {{ __('Save') }}
                </x-jet-button>
            </x-slot>
        </x-jet-form-section>
    </div>
</div>
