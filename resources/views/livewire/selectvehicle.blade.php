<div class="p-6 sm:px-20 bg-white border-b border-gray-200">
    {{-- Image --}}
    <div class="grid grid-cols-3 gap-4">
        @foreach ($vehicles as $vehicle)
            <div class="flex justify-center">
                <img class="h-36" src={{ $vehicle['imglink'] }} />
            </div>
        @endforeach
    </div>
    {{-- name/model --}}
    <div class="grid grid-cols-3 gap-4 font-extrabold">
        @foreach ($vehicles as $vehicle)
            <div class="flex justify-center ">
                {{ $vehicle['name'] }}
            </div>
        @endforeach
    </div>
    {{-- price --}}
    <div class="grid grid-cols-3 gap-4">
        @foreach ($vehicles as $vehicle)
            <div class="flex justify-center">
                ex-showroom&nbsp;<span class="">₹{{ $vehicle['exshowroom'] }}</span>
            </div>
        @endforeach
    </div>
    {{-- input number --}}
    <div class="grid grid-cols-3 gap-4 font-extrabold">
        @foreach ($vehicles as $vehicle)
            <div class="flex justify-center mt-3">
                <div class="col-span-6 sm:col-span-4">
                    <x-jet-label for="qty{{ $loop->index }}" value=" {{ __('No. of vehicles') }}" />
                    <x-jet-input wire:model="vehicles.{{ $loop->index }}.qty" id="qty{{ $loop->index }}"
                        type="number" min="0" class="mt-1 block w-full" autofocus />
                    <x-jet-input-error for="qty{{ $loop->index }}" class=" mt-2" />
                </div>
            </div>
        @endforeach
    </div>
    {{-- application button --}}
    <div class="grid bg-gray-200 py-3 grid-cols-3 mt-8 justify-items-center">
        <div>
            <x-jet-label for="bla" value=" {{ __('Total asset cost') }}" />
            <span class="mt-1 block w-full text-lg font-extrabold">₹{{ $totalassetcost }}</span>
        </div>
        <div>
            <x-jet-label for="marginmoney" value=" {{ __('Margin-money') }}" />
            <x-jet-input wire:model="marginmoney" id="marginmoney" type="text" class="mt-1 block w-full" autofocus />
            <x-jet-input-error for="marginmoney" class=" mt-2" />
            <span
                class="@if ($this->marginmoney < $this->totalassetcost * 0.2)
                font-extrabold text-red-700
                @else
                font-extralight
            @endif ">{{ $minmarginmoneyline }}</span>
        </div>
        <div>
            <x-jet-label for="tenure" value="{{ __('Tenure (months)') }}" />
            <x-jet-button id="btntenure1" class="" wire:click="$set('tenure',24)">24</x-jet-button>
            <x-jet-button id="btntenure2" class="ml-3" wire:click="$set('tenure',36)">36</x-jet-button>
            <x-jet-button id="btntenure3" class="ml-3" wire:click="$set('tenure',48)">48</x-jet-button>
        </div>
    </div>
    <div class=" bg-gray-200 grid grid-cols-3 py-3 justify-items-center">
        <div>
            <x-jet-label for="loanamount" value="{{ __('Loan amount') }}" />
            <span class="mt-1 block w-205px text-lg font-extrabold">₹{{ $principalamount }}</span>
        </div>
        <div>
            <x-jet-label class="w-205px" for="emi" value="{{ __('EMI') }}" />
            <span class="mt-1 block w-205px text-lg font-extrabold">₹{{ $emi }}</span>
        </div>
        <div>
            <x-jet-label for="emi" value="{{ __('Tenure') }}" />
            <span class="mt-1 block w-205px text-lg font-extrabold">{{ $tenure }} months</span>
        </div>

    </div>
    <div class="grid  mt-2 mb-3 justify-items-center">
        @if ($principalamount)
            @if ($this->marginmoney < $this->totalassetcost * 0.2)
                <x-jet-button disabled onclick="location.href='{{ url('application') }}'">Apply for credit
                </x-jet-button>
            @else
                <x-jet-button onclick="location.href='{{ url('application') }}'">Apply for credit</x-jet-button>
            @endif
        @else
            <x-jet-button disabled onclick="location.href='{{ url('application') }}'">Select vehicles to apply for
                credit</x-jet-button>
        @endif
    </div>
</div>
