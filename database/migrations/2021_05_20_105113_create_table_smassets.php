<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSmassets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smassets', function (Blueprint $table) {
            $table->id();
            $table->date('dateOnboarded');
            $table->string('assetID');
            $table->string('batteryData', 2000);
            $table->string('customerName');
            $table->string('phone');
            $table->integer('odometer')->unsigned();
            $table->dateTime('lastSwapDone');
            $table->string('vehicleStatus');
            $table->integer('activityIndex')->unsigned();
            $table->dateTime('markForDefault');
            $table->string('automationLevel');
            $table->string('callToAction');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_smassets');
    }
}
