<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableApplicationsAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Applications', function (Blueprint $table) {
            $table->string("name");
            $table->string("email");
            $table->string("phone");
            $table->string("nameofcompany");
            $table->string("companytype");
            $table->string("revenue");
            $table->string("panno");
            $table->string("dob");
            $table->string("pangender");
            $table->string("pincode");
            $table->string("bankstatementfile");
            $table->string("pancardfile");
            $table->string("companypancardfile");
            $table->string("aadharfile");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("name");
            $table->dropColumn("email");
            $table->dropColumn("phone");
            $table->dropColumn("nameofcompany");
            $table->dropColumn("companytype");
            $table->dropColumn("revenue");
            $table->dropColumn("panno");
            $table->dropColumn("dob");
            $table->dropColumn("pangender");
            $table->dropColumn("pincode");
            $table->dropColumn("bankstatementfile");
            $table->dropColumn("pancardfile");
            $table->dropColumn("companypancardfile");
            $table->dropColumn("aadharfile");
        });
    }
}
