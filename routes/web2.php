<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/application', function () {
    return view('applform');
})->name('applform');


Route::middleware(['auth:sanctum', 'verified'])->get('/applications', function () {
    return view('applgrid');
})->name('applgrid');


Route::middleware(['auth:sanctum', 'verified'])->get('/formsubmitted', function () {
    return view('formsubmitted');
})->name('formsubmitted');

Route::middleware(['auth:sanctum', 'verified'])->get('/offers', function () {
    return view('offers');
})->name('offers');

Route::middleware(['auth:sanctum', 'verified'])->get('/assets', function () {
    return view('assetgridmain');
})->name('assets');

Route::middleware(['auth:sanctum', 'verified'])->get('/applaccepted', function () {
    return view('applaccepted');
})->name('applaccepted');
